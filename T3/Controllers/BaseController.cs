﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T3.Controllers
{
    public class BaseController
    {
        
        private readonly T3Context context;
        public BaseController(T3Context context)
        {
            this.context = context;
        }

        protected Usuario LoggedUser()
        {
            var claim = HttpContext.User.Claims.FirstOrDefault();
            var user = context.Usuarios.Where(o => o.Nombre == claim.Value).FirstOrDefault();
            return user;
        }
    }
}
}
